import './Table.css'
const Table = ({ people }) => {

    const table = () => people.map((persone, i) => {// link for email
        return (
            <tr key={people.email}>
                <td>
                    <img src={`http://apis.chromeye.com:9191${persone?.avatar?.url}`} alt='profile image' className='profile-image' />
                </td>
                <td>{persone?.id}</td>
                <td>{persone?.firstName}</td>
                <td>{persone?.lastName}</td>
                <td>{persone?.email}</td>
                <td>{persone?.company.name}</td>
                <td>{persone?.company.department}</td>
                <td>{persone?.company.startDate}</td>
            </tr>
        )
    });

    return (
        <div className='Table'>
            <table cellspacing="0">
                <tr>
                    <th>Avatar</th>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Company</th>
                    <th>Department</th>
                    <th>Start Date</th>
                </tr>
                {table()}
            </table>
        </div>
    );
}

export default Table;