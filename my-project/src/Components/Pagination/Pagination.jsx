import { Button, ButtonGroup, InputLabel, FormControl, NativeSelect, } from '@mui/material';
import { Box } from '@mui/system';
import React from 'react';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import './Pagination.css';
const perPageArray = [1, 3, 5];

const Pagination = ({ people, setPageData }) => {

  const [page, setPage] = React.useState(1);
  const [numberPerPage, setNumberPerPage] = React.useState(5);

  React.useEffect(() => {
    if (numberPerPage === 0)
      setPageData(people);
    else {
      const startIndex = numberPerPage * (page - 1);
      const endIndex = numberPerPage * page;
      setPageData(people.slice(startIndex, endIndex));
    }

  }, [page, numberPerPage, people, setPageData]);


  const pageOptions = React.useMemo(() => {
    if (numberPerPage === 0) return [1];
    const pages = [];
    const numberLastPage = Math.ceil(people.length / numberPerPage);

    for (let i = 1; i <= numberLastPage; i++) {
      pages.push(i);
    }

    return pages;

  }, [numberPerPage, people]);


  const changePerPage = (e) => {
    setNumberPerPage(Number(e.target.value));
    setPage(1);
  }


  return (
    <div className='pagination-perPage'>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          height: 130,
          '& > *': {
            m: 1,
          },
        }}
      >
        {pageOptions.map(page => {
          return (
            <ButtonGroup variant="outlined" aria-label="outlined button group" key={page}>
              <Button onClick={() => setPage(page)}>{page}</Button>
            </ButtonGroup>
          );
        })}
      </Box>
      <div className='arrow'>
        {(page === 1) ?
          <div><ArrowForwardIosIcon onClick={() => pageOptions.length === 1 ? setPage(page) : setPage(page + 1)} /></div> :
          ((page === pageOptions[pageOptions.length - 1]) ?
            <div><ArrowBackIosIcon onClick={() => setPage(page - 1)} /></div> :
            (<div><ArrowBackIosIcon onClick={() => setPage(page - 1)} /><ArrowForwardIosIcon onClick={() => setPage(page + 1)} /></div>)
          )
        }
      </div>



      <Box sx={{ minWidth: 120, width: 10 }}>
        <FormControl variant="standard">
          <InputLabel autoWidth htmlFor="uncontrolled-native">
            Per Page
          </InputLabel>
          <NativeSelect
            onChange={e => changePerPage(e)}
            defaultValue={5}
            inputProps={{
              name: 'perPage',
              id: 'uncontrolled-native',
            }}
          >
            {perPageArray.map(option => {
              return (<option value={option}>{`---${option}---`}</option>)
            })}
            <option value={0}>{`---All---`}</option>
          </NativeSelect>
        </FormControl>
      </Box>
    </div>
  );
}
export default Pagination;