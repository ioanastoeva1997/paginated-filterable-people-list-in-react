import { TextField } from "@mui/material";
import { Box } from "@mui/system";
import React from 'react';

const SearchBox = ({ people, setFilterPeople }) => {

  const [search, setSearch] = React.useState('');

  React.useEffect(() => {
    const filterPeople = people
      .filter((persone) =>
        persone.firstName.toLowerCase().includes(search.toLowerCase()) ||
        persone.lastName.toLowerCase().includes(search.toLowerCase()));
    setFilterPeople(filterPeople);

    if (search.length > 0 && filterPeople.length === 0) document.getElementById("dontHavePeople").innerHTML = 'No matched people found';
    else document.getElementById("dontHavePeople").innerHTML = '';

  }, [search, people]);

  return (
    <div className='searchBox-pagination-perPage'>
      <Box
        component="form"
        sx={{
          '& > :not(style)': { m: 1, width: 2500 },
        }}
        autoComplete="off"
      >
        <TextField id="outlined-basic" label="Enter Keyword" variant="outlined" onChange={e => setSearch(e.target.value)} />
        <div id='dontHavePeople'></div>
      </Box>
    </div>
  );
}

export default SearchBox;