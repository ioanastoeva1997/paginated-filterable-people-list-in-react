const request = async () => {
  const a = await fetch('http://apis.chromeye.com:9191/people', {
    method: 'GET',
    headers: {
      'content-type': 'application/json'
    }
  });
  return a.json()
}
export { request };