import './App.css';
import * as React from 'react';
import { request } from './data/data';
import SearchBox from './Components/SearchBox/SearchBox';
import Pagination from './Components/Pagination/Pagination';
import Table from './Components/Table/Table';


const App = () => {

  const [people, setPeople] = React.useState([]);
  const [pageData, setPageData] = React.useState([]);
  const [filterPeople, setFilterPeople] = React.useState([]);

  React.useEffect(() => {
    request().then(
      data => {
        setPeople(data);
      });
  }, []);

  return (
    <div className="App">
      <div className='pagination-perPage'>
        <SearchBox people={people} setFilterPeople={setFilterPeople} />
        <Pagination people={filterPeople} setPageData={setPageData} />
      </div>
      <Table people={pageData} />
    </div>
  );
}

export default App;
